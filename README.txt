This module is a tool for the Notify module (https://drupal.org/project/notify). This is a small module that simply tells the Views module about the Notify database so you can create listings of users by notify subscription status for export or administration.

There is no administration. Simply create a view of type "User" and you'll see that under "Fields" and "Filters" there are additional options under the category "Notify."

I used this module to make a listing of node subscribers with names and email addresses and then exported that list using the Views Data Export module. (https://drupal.org/project/views_data_export)

This module is not going to break your site or anything, it's just not fully completed and only offers a simple thing, which is why it's in sandbox.